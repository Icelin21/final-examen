const oscuro = document.querySelectorAll(".bg-light, .texto-blanco")
const textOscuro = document.querySelectorAll(".text-dark, .navbar-dark, .btn-dark, .bg-dark")
const navbar = document.querySelector("nav.bg-dark")

var cont = 0;

function dark() {
    cont++
    
    oscuro.forEach(function(a) {
        a.classList.toggle("bg-dark")
        a.classList.toggle("texto-blanco")
    })
    
    if(cont == 1) {
        navbar.classList.replace("bg-dark", "bg-light")
        console.log(textOscuro)
        textOscuro.forEach(function(b) {
            b.classList.replace("navbar-dark", "navbar-light")
            b.classList.replace("text-dark", "text-light")
            b.classList.replace("btn-dark", "btn-light")
            b.classList.replace("bg-dark", "bg-light")
        })
    } else if (cont == 2) {
        for (let i=0; i<textOscuro.length; i++) {
            textOscuro[i].classList.replace("text-light", "text-dark") 
            textOscuro[i].classList.replace("navbar-light", "navbar-dark")
            textOscuro[i].classList.replace("btn-light", "btn-dark")
            textOscuro[i].classList.replace("bg-light", "bg-dark")
        }
        navbar.classList.replace("bg-light", "bg-dark")
        cont = 0    
    }

}